/*
 * @Author: your name
 * @Date: 2020-06-03 21:25:25
 * @LastEditTime: 2020-12-04 13:51:30
 * @LastEditors: zhang zi fang
 * @Description: In User Settings Edit
 * @FilePath: \nodec:\Users\zhamgzifang\Desktop\app\src\main.js
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from './axios'
import 'highlight.js/styles/monokai-sublime.css'
import viewTheInterToGnerateJSON from './views/TheInterToGnerateJSON'
import moment from 'moment'
Vue.prototype.$moment = moment
Vue.prototype.$http = axios
Vue.use(ElementUI);
Vue.config.productionTip = false
Vue.component('TheInterToGnerateJSON', viewTheInterToGnerateJSON)
Vue.prototype.loc = {
  get(v){
    return localStorage[v] ? JSON.parse(localStorage[v]) : ''
  },
  set(v,s,v3){
    if(v3){
      var _v = localStorage[v] ? JSON.parse(localStorage[v]) : {}
      _v[s] = v3
      s = _v
    }
    localStorage[v] = JSON.stringify(s)
  }
}
new Vue({
  router,
  data() {
    return {
      webpageView:false, //网页版
      surfaceRelevance:[] //表关联
    }
  },
  store,
  render: h => h(App)
}).$mount('#app')
