/*
 * @Descripttion: 
 * @version: 
 * @Author: miss zhang
 * @Date: 2020-06-15 12:47:49
 * @LastEditors: zhang zi fang
 * @LastEditTime: 2020-08-14 16:54:01
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    surfaceRelevanceSelect: []
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
